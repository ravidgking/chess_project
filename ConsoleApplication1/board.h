// Board.h
#pragma once
#include <string>

class Board {
private:
    char board[8][8];  // Assuming an 8x8 chess board
    static const int rows = 8;  // Define the number of rows
    static const int cols = 8;  // Define the number of columns

public:
    Board();
    void initializeBoard();
    void displayBoard();
    std::string getBoardString();  // New method to get the board state as a string
    std::string getPieceType(char file, char rank);  // New method to get the type of piece at a specific location
    void movePiece(int srcRow, int srcCol, int destRow, int destCol);
    bool isValidMove(int srcRow, int srcCol, int destRow, int destCol);
    bool hasLegalMoves(int row, int col);
    bool isKingInCheck(int kingRow, int kingCol, int playerColor);
    
};
