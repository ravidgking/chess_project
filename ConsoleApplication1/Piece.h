// Piece.h
#pragma once
#include <string>
#include <utility>

class Piece {
protected:
    std::string type;
    int color;
    std::pair<int, int> position;  // Add this line to declare the position variable

public:
    Piece(std::string pieceType, int pieceColor);
    virtual bool move(std::string move) = 0;
    std::pair<int, int> parsePosition(std::string move);  // Add this line to declare the parsePosition function
};

class King : public Piece {
public:
    King(int pieceColor);
    bool move(std::string move) override;
};

class Queen : public Piece {
public:
    Queen(int pieceColor);
    bool move(std::string move) override;
};

class Bishop : public Piece {
public:
    Bishop(int pieceColor);
    bool move(std::string move) override;
};

class Rook : public Piece {
public:
    Rook(int pieceColor);
    bool move(std::string move) override;
};

class Knight : public Piece {
public:
    Knight(int pieceColor);
    bool move(std::string move) override;
};

class Pawn : public Piece {
public:
    Pawn(int pieceColor);
    bool move(std::string move) override;
};
