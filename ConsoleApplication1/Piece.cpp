// Piece.cpp
#include "Piece.h"

Piece::Piece(std::string pieceType, int pieceColor) : type(pieceType), color(pieceColor) {
    // Constructor implementation
    // YOUR CODE: Perform any additional initialization if needed
}
std::pair<int, int> Piece::parsePosition(std::string move) {
    // Convert the column letter to a number
    char colChar = move[0];
    int colNum = colChar - 'A';

    // Convert the row number to an integer
    int rowNum = std::stoi(move.substr(1));

    // Subtract 1 from both numbers because chessboard indices start at 0
    return { rowNum - 1, colNum - 1 };
}


bool King::move(std::string move) {
    // Parse the move string into start and end positions
    std::pair<int, int> start = parsePosition(move);
    std::pair<int, int> end = parsePosition(move);

    // Check if the move is valid for a king
    if (abs(start.first - end.first) <= 1 && abs(start.second - end.second) <= 1) {
        // Move the king
        position = end;
        return true;
    }
    else {
        // Invalid move
        return false;
    }
}
bool Queen::move(std::string move) {
    // Parse the move string into start and end positions
    std::pair<int, int> start = parsePosition(move);
    std::pair<int, int> end = parsePosition(move);

    // Check if the move is valid for a queen
    if ((start.first == end.first || start.second == end.second) ||
        (abs(start.first - end.first) == abs(start.second - end.second))) {
        // Move the queen
        position = end;
        return true;
    }
    else {
        // Invalid move
        return false;
    }
}



bool Bishop::move(std::string move) {
    // Parse the move string into start and end positions
    std::pair<int, int> start = parsePosition(move);
    std::pair<int, int> end = parsePosition(move);

    // Check if the move is valid for a bishop
    if (abs(start.first - end.first) == abs(start.second - end.second)) {
        // Move the bishop
        position = end;
        return true;
    }
    else {
        // Invalid move
        return false;
    }
}

bool Rook::move(std::string move) {
    // Parse the move string into start and end positions
    std::pair<int, int> start = parsePosition(move);
    std::pair<int, int> end = parsePosition(move);

    // Check if the move is valid for a rook
    if (start.first == end.first || start.second == end.second) {
        // Move the rook
        position = end;
        return true;
    }
    else {
        // Invalid move
        return false;
    }
}


bool Knight::move(std::string move) {
    // Parse the move string into start and end positions
    std::pair<int, int> start = parsePosition(move);
    std::pair<int, int> end = parsePosition(move);

    // Check if the move is valid for a knight
    if ((abs(start.first - end.first) == 2 && abs(start.second - end.second) == 1) ||
        (abs(start.first - end.first) == 1 && abs(start.second - end.second) == 2)) {
        // Move the knight
        position = end;
        return true;
    }
    else {
        // Invalid move
        return false;
    }
}

bool Pawn::move(std::string move) {
    // Parse the move string into start and end positions
    std::pair<int, int> start = parsePosition(move);
    std::pair<int, int> end = parsePosition(move);

    // Check if the move is valid for a pawn
    if (start.first + 1 == end.first || (start.first == 1 && start.first + 2 == end.first)) {
        // Move the pawn
        position = end;
        return true;
    }
    else {
        // Invalid move
        return false;
    }
}

