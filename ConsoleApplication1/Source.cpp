// Add the necessary includes
#include "ChessGame.h"
#include "Board.h"
#include "Player.h"
#include "Pipe.h"
#include <iostream>
#include <thread>

using std::cout;
using std::endl;
using std::string;

// Function to handle the move validation result
string handleMoveResult(int result) {
    switch (result) {
    case 0:
        return "0"; // Valid move
    case 1:
        return "1"; // Valid move, captured opponent's piece
    case 2:
        return "2"; // Invalid move, no piece in the source square
    case 3:
        return "3"; // Invalid move, destination square occupied by own piece
    case 4:
        return "4"; // Invalid move, move results in self-check
    case 5:
        return "5"; // Invalid move, invalid square indices
    case 6:
        return "6"; // Invalid move, illegal piece movement
    case 7:
        return "7"; // Invalid move, source and destination squares are the same
    case 8:
        return "8"; // Valid move, checkmate
    default:
        return "Invalid result";
    }
}

int main() {
    // Create instances of ChessGame, Board, Player, and Pipe
    ChessGame game;
    Board board;
    Player player(0); // Black starts
    Pipe pipe;

    // Connect to the graphics
    bool isConnected = pipe.connect();

    if (!isConnected) {
        cout << "Unable to connect to graphics. Exiting..." << endl;
        return 1;
    }

    // Initialize the board and send it to the graphics
    board.initializeBoard();
    string initialBoard = board.getBoardString();
    pipe.sendMessageToGraphics(initialBoard.c_str());

    // Get the initial message from graphics
    string msgFromGraphics = pipe.getMessageFromGraphics();

    while (msgFromGraphics != "quit") {
        // Process the received message (move) from graphics
        int moveResult = game.processMove(msgFromGraphics);

        // Handle the move result and send the result back to graphics
        string resultMessage = handleMoveResult(moveResult);
        pipe.sendMessageToGraphics(resultMessage.c_str());

        // Get the next move from graphics
        msgFromGraphics = pipe.getMessageFromGraphics();
    }

    // Close the connection
    pipe.close();

    return 0;
}
