// ChessGame.h
#pragma once
#include <string>

class ChessGame {
private:
    Board* board;
    Player* currentPlayer;

public:
    ChessGame();
    ~ChessGame();
    void startGame();
    int processMove(const std::string& moveString);  // Corrected function signature
    void switchPlayer();
};