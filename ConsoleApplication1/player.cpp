// Player.cpp
#include "Player.h"
#include <string>  // Include this line to use std::to_string

Player::Player(int playerColor) : color(playerColor) {
    // Constructor implementation
    // You might initialize some variables here, depending on your game design
}

bool Player::isValidMove(std::string source, std::string destination) {
    // Check if the move is within the chessboard's boundaries
    // Add more validation logic based on your game rules
    // This is a placeholder implementation
    return source.length() == 2 && destination.length() == 2;
}

std::string Player::makeMove(std::string source, std::string destination) {
    // Implementation to make a move by the player
    // Validate the move and update the game state accordingly.

    if (isValidMove(source, destination)) {
        std::cout << "Player " << color << " moved from " << source << " to " << destination << std::endl;
        return "Player " + std::to_string(color) + " moved from " + source + " to " + destination;
    }
    else {
        std::cout << "Invalid move! Try again." << std::endl;
        return "Invalid move! Try again.";
    }
}