// Board.cpp
#include "Board.h"
#include <iostream> 
Board::Board() {
    initializeBoard();
}

void Board::initializeBoard() {
    // Initialize the chessboard with pieces
    // For simplicity, we'll use FEN notation for the starting position
    // 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR'
    std::string startingPosition =
        "rnbqkbnr"
        "pppppppp"
        "        "
        "        "
        "        "
        "        "
        "PPPPPPPP"
        "RNBQKBNR";

    int index = 0;
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            board[i][j] = startingPosition[index++];
        }
    }
}

void Board::displayBoard() {
    // Display the chessboard
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            std::cout << board[i][j] << ' ';
        }
        std::cout << std::endl;
    }
}

std::string Board::getPieceType(char file, char rank) {
    // Function to get the type of piece at a specific location
    // Convert file and rank to array indices
    int row = rank - '1';
    int col = file - 'a';

    // Check for valid indices
    if (row >= 0 && row < rows && col >= 0 && col < cols) {
        return std::string(1, board[row][col]);
    }

    // Invalid indices, return an empty string
    return "";
}
std::string Board::getBoardString() {
    std::string boardString;
    for (int i = 0; i < 8; ++i) {
        for (int j = 0; j < 8; ++j) {
            boardString.push_back(board[i][j]);
        }
    }
    return boardString;
}
bool Board::isValidMove(int srcRow, int srcCol, int destRow, int destCol) {
    // Check if the source and destination squares are within the bounds of the board
    if (srcRow < 0 || srcRow >= rows || srcCol < 0 || srcCol >= cols || destRow < 0 || destRow >= rows || destCol < 0 || destCol >= cols) {
        return false;
    }

    // Check if the source square contains a piece
    if (board[srcRow][srcCol] == ' ') {
        return false;
    }

    // Check if the destination square is occupied by a piece of the same color
    if (board[destRow][destCol] != ' ' && board[srcRow][srcCol] == board[destRow][destCol]) {
        return false;
    }

    // TODO: Add more checks to validate the specific movement rules for each piece type

    return true;
}
bool Board::isKingInCheck(int kingRow, int kingCol, int playerColor) {
    // Iterate through the board to find opponent pieces
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            // Check if the piece at (i, j) is of the opponent color
            if (board[i][j] != ' ' && getPlayerColor(board[i][j]) != playerColor) {
                // Check if the opponent piece can attack the king
                if (isValidMove(i, j, kingRow, kingCol)) {
                    return true; // King is in check
                }
            }
        }
    }

    return false; // King is not in check
}
void Board::movePiece(int srcRow, int srcCol, int destRow, int destCol) {
    // Move the piece from the source square to the destination square
    board[destRow][destCol] = board[srcRow][srcCol];
    board[srcRow][srcCol] = ' ';
}

