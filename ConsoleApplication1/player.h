// Player.h
#pragma once

#include <iostream>

class Player {
private:
    int color;

public:
    Player(int playerColor);
    int getColor() const;  // Add this line to declare the getColor function
    bool isValidMove(std::string source, std::string destination);  // Add this line to declare the isValidMove function
    std::string makeMove(std::string source, std::string destination);  // Add this line to declare the makeMove function
};