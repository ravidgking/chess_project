#include "ChessGame.h"
#include "Board.h"
#include "Player.h"
#include <iostream>

ChessGame::ChessGame() {
    // Initialize the board and players
    board = new Board();
    currentPlayer = new Player(0); // Black starts
}

ChessGame::~ChessGame() {
    delete board;
    delete currentPlayer;
}

void ChessGame::startGame() {
    // Start the game
    std::cout << "Starting game..." << std::endl;
}

int ChessGame::processMove(const std::string& move) {
    // Parse the move string into source and destination coordinates
    int srcRow = move[1] - '1';
    int srcCol = move[0] - 'a';
    int destRow = move[3] - '1';
    int destCol = move[2] - 'a';

    // Check if the move is valid
    if (!board->isValidMove(srcRow, srcCol, destRow, destCol)) {
        return 2; // Invalid move, no piece in the source square
    }

    // Perform the move
    board->movePiece(srcRow, srcCol, destRow, destCol);

    // Check if the move resulted in checkmate
    if (board->isKingInCheck(destRow, destCol, currentPlayer->getColor())) {
        // Check if the king has legal moves
        if (!board->hasLegalMoves(destRow, destCol)) {
            return 8; // Checkmate
        }
    }

    // The move was successful
    return 0;
}

void ChessGame::switchPlayer() {
    // Switch the current player
    if (currentPlayer->getColor() == 0) {
        currentPlayer = new Player(1); // White is 1
    }
    else {
        currentPlayer = new Player(0); // Black is 0
    }
}
